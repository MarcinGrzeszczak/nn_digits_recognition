const currentColor = "black";
const currentSize = 20;

const canvas = document.getElementById("can");
const clearBtn = document.getElementById("clearBtn");
const saveBtn = document.getElementById("saveBtn");
const resultContainer = document.getElementById('result');

const ctx = canvas.getContext("2d");
let isMouseDown = false;
canvas.addEventListener("mousedown", event => startdraw(event));
canvas.addEventListener("mousemove", event => draw(event));
canvas.addEventListener("mouseup", () => stopDraw());
clearBtn.addEventListener("click", clear);
saveBtn.addEventListener("click", save);

function startdraw(event) {
  isMouseDown = true;
  const currentPosition = getMousePos(event);
  ctx.moveTo(currentPosition.x, currentPosition.y);
  ctx.beginPath();
  ctx.lineWidth = currentSize;
  ctx.lineCap = "round";
  ctx.strokeStyle = currentColor;
}

function draw(event) {
  if (isMouseDown) {
    var currentPosition = getMousePos(event);
    ctx.lineTo(currentPosition.x, currentPosition.y);
    ctx.stroke();
  }
}

function stopDraw() {
  isMouseDown = false;
}

function getMousePos(evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function clear() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function save() {
  const scaledCanvas = document.createElement("canvas");
  const scaledCtx = scaledCanvas.getContext("2d");

  scaledCanvas.width = 28;
  scaledCanvas.height = 28;
  scaledCtx.scale(28.0 / canvas.width, 28.0 / canvas.height);
  scaledCtx.drawImage(canvas, 0, 0);
  //document.appendChild(scaledCanvas)
  const imgData = scaledCtx.getImageData(
    0,
    0,
    scaledCanvas.width,
    scaledCanvas.height
  );
  const pixels = imgData.data;

  // var grayscaleImg = [];
  // for (var y = 0; y < imgData.height; y++) {
  //   grayscaleImg[y]=[];
  //   for (var x = 0; x < imgData.width; x++) {
  //     var offset = y * 4 * imgData.width + 4 * x;
  //     var alpha = pixels[offset+3];
  //     // weird: when painting with stroke, alpha == 0 means white;
  //     // alpha > 0 is a grayscale value; in that case I simply take the R value
  //     if (alpha == 0) {
  //       pixels[offset] = 255;
  //       pixels[offset+1] = 255;
  //       pixels[offset+2] = 255;
  //     }
  //     pixels[offset+3] = 255;
  //     // simply take red channel value. Not correct, but works for
  //     // black or white images.
  //     grayscaleImg[y * x] = pixels[y*4*imgData.width + x*4 + 0] / 255;
  //   }
  // }
  // console.log(grayscaleImg)
  
 
  let grayscale = [];
  for (let i = 0; i < pixels.length; i += 4) {
    grayscale.push(pixels[i + 3] / 255);
  }
  scaledCtx.restore();
  fetchData(grayscale, scaledCanvas)
  console.log(imgData);
}

function fetchData(data, canv) {
  fetch("predict",
    {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({data})
})
  .then(res => res.json())
  .then(json => {
    console.log(json)
    // const container  = document.createElement('div')
    // document.body.appendChild(container)
    // container.appendChild(canv);
    // //container.innerText = json.maxval
   
  })
  .catch(function(res){ console.log(res) })
}