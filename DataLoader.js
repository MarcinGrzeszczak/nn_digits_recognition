const csv = require('csv-parser');
const Files = require('./Files')

async function loadTestData(path) {
    let results = []
    const parser = Files.readStream(path).pipe(csv());
    for await (const row of parser) {
        results.push(Object.values(row).map(val => val /255))
    }
    return results
}

async function loadData(parsedPath, csvPath) {
    let preparedData = [];
    if(!Files.isFileExists(parsedPath)) {
        let results = []
        console.log('preparing data ...')
        
       const parser = Files.readStream(csvPath).pipe(csv());

        for await (const row of parser) {
            preparedData.push({
                label: row.label,
                data: Object.values(row).filter((val,index) => index !== 0).map(val => parseInt(val) / 255)
            });
        }
        console.log('Finished preparing.')
        console.log('Saving...')
        Files.saveSync(parsedPath, preparedData);
        console.log('Saved.')
    } else {
        console.log('Loading data from file ...')
        preparedData = await Files.read(parsedPath);
        console.log('Finished loading.')
    }

    return preparedData
}

module.exports = {
    loadData,
}