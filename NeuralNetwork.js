const mathjs = require('mathjs');
const Files = require('./Files')
const Logger = require('./Logger');

const WEIGHTS_PATH = 'weights.data';

const errRateLogger = new Logger();
const weightsLogger = new Logger();
const settingsLogger = new Logger();
const accuracyLogger = new Logger();
let accuracy = [];

class NeuralNetwork {
    constructor(inputNodes, hidden1Nodes, hidden2Nodes, outputNodes, epochs ,learningRate) {
        this.inputNodes = inputNodes;
        this.hidden1Nodes = hidden1Nodes;
        this.hidden2Nodes = hidden2Nodes;
        this.outputNodes = outputNodes;
        
        this.activation = tanh;
        this.epochs = epochs || 10;
        this.learningRate = learningRate || .5;
        
        this.hidden1Layer = Array(this.hidden1Nodes).fill(0);
        this.hidden2Layer = Array(this.hidden2Nodes).fill(0);
        this.outputLayer = Array(this.outputNodes).fill(0);

        this.synapseIH1 =  mathjs.random([this.inputNodes, this.hidden1Nodes], -1.0, 1.0);
        this.synapseH1H2 = mathjs.random([this.hidden1Nodes, this.hidden2Nodes], -1.0, 1.0);
        this.synapseH2O = mathjs.random([this.hidden2Nodes, this.outputNodes], -1.0, 1.0);
    }

    async loadWheights() {
        const weights = await Files.read(WEIGHTS_PATH) 
        this.synapseIH1 = mathjs.matrix(weights[0].data);
        this.synapseH1H2 = mathjs.matrix(weights[1].data);
        this.synapseH2O = mathjs.matrix(weights[2].data);
    }

    train(trainData) {
        this.saveStartSettings();
        let outputError = 0;
        let picture = 0;
        for(let i = 0; i < this.epochs; i++) {

            for (const row of trainData) {
                const inputLayer = mathjs.matrix([row.data]);
                const prepareTarget = Array(10).fill(0);
                prepareTarget[row.label] = 1;
                const target = mathjs.matrix([prepareTarget])
  
                this.predict(row.data)
               
                accuracy.push(row.label == this.outputLayer._data[0].findIndex(val => val === mathjs.max(this.outputLayer)));
                
                //console.log('accuracy: ' + accuracy.filter(val => val === true).length / accuracy.length * 100 + '%')

                outputError = mathjs.subtract(target, mathjs.matrix(this.outputLayer));
                const outputDelta = mathjs.dotMultiply(outputError, this.outputLayer.map(v => this.activation(v, true)));

                const hidden2Error = mathjs.multiply(outputError, mathjs.transpose(this.synapseH2O));
                const hidden2Delta = mathjs.dotMultiply(hidden2Error, this.hidden2Layer.map(v => this.activation(v, true)));

                const hidden1Error = mathjs.multiply(hidden2Error, mathjs.transpose(this.synapseH1H2));
                const hidden1Delta = mathjs.dotMultiply(hidden1Error, this.hidden1Layer.map(v => this.activation(v, true)));
                
                //gradient
                this.synapseH2O = mathjs.add(this.synapseH2O, mathjs.multiply(mathjs.transpose(this.hidden2Layer), mathjs.multiply(outputDelta, this.learningRate)));
                this.synapseH1H2 = mathjs.add(this.synapseH1H2, mathjs.multiply(mathjs.transpose(this.hidden1Layer), mathjs.multiply(hidden2Delta, this.learningRate)));
                this.synapseIH1 = mathjs.add(this.synapseIH1, mathjs.multiply(mathjs.transpose(inputLayer), mathjs.multiply(hidden1Delta, this.learningRate)));
                errRateLogger.log(`picture ${picture++}: ${mathjs.mean(mathjs.abs(outputError))}`)
            }
            picture = 0;
            errRateLogger.log(`epoch ${i}: ${mathjs.mean(mathjs.abs(outputError))}`)
            errRateLogger.saveSync(`logs/epoch_${i}.log`)
            console.log('saving weights')
            weightsLogger.log([this.synapseIH1, this.synapseH1H2, this.synapseH2O])
            weightsLogger.saveSync('logs/weights.log')
            Files.saveSync(WEIGHTS_PATH, [this.synapseIH1, this.synapseH1H2, this.synapseH2O]);
        }
    }

    test(testData) {
        accuracy = [];
        for (const row of testData) {
            const out = this.predict(row.data)
            const isRecognized = row.label == out._data[0].findIndex(val => val === mathjs.max(this.outputLayer));
            accuracy.push(isRecognized);
  
            if(!isRecognized) {
              Files.saveCanvas(`unrecognized/${accuracy.length}.jpg`, row.data);
            }
            
            console.log(' [TESTING] accuracy: ' + accuracy.filter(val => val === true).length / accuracy.length * 100 + '%')
        }
        this.saveAccuracy();
    }


    predict(input) {
        const inputLayer = mathjs.matrix([input])
        this.hidden1Layer = mathjs.multiply(inputLayer, this.synapseIH1).map(v => this.activation(v, false));
        this.hidden2Layer = mathjs.multiply(this.hidden1Layer, this.synapseH1H2).map(v => this.activation(v, false));
        this.outputLayer = mathjs.multiply(this.hidden2Layer, this.synapseH2O).map(v => this.activation(v, false));
        return this.outputLayer;
    }

    saveAccuracy() {
        accuracyLogger.log('accuracy: ' + accuracy.filter(val => val === true).length / accuracy.length * 100 + '%')
        accuracyLogger.save('logs/accuracy.log')
        accuracy = [];
    }

    saveStartSettings() {
        settingsLogger.log(`inputNodes: ${this.inputNodes}`)
        settingsLogger.log(`hidden1Nodes: ${this.hidden1Nodes}`)
        settingsLogger.log(`hidden2Nodes: ${this.hidden2Nodes}`)
        settingsLogger.log(`outputNodes: ${this.outputNodes}`)
        settingsLogger.log(`epochs: ${this.epochs}`)
        settingsLogger.log(`learningRate: ${this.learningRate}`)
    
        settingsLogger.saveSync('logs/settings.log')
    }
}

function sigmoid(x, derivative) {
    const fx = 1 / (1 + mathjs.exp(-x));
    if(derivative) {
        return fx * (1 - fx);
    }
    return fx;
}

function softmax(x, derivative) {
   const exps = mathjs.exp( mathjs.subtract(x, mathjs.max(x)));
   return mathjs.divide(exps, mathjs.sum(exps)); 
}

function tanh(x, derivative) {
    if(derivative) {
        return 1-(x*x)
    }
    return Math.tanh(x)
}


module.exports = NeuralNetwork;