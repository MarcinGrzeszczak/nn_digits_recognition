// const winston = require('winston');
 
// const logger = winston.createLogger({
//   level: 'info',
//   format: winston.format.json(),
//   defaultMeta: { service: 'user-service' },
//   transports: [
//     //
//     // - Write all logs with level `error` and below to `error.log`
//     // - Write all logs with level `info` and below to `combined.log`
//     //
//     new winston.transports.File({ filename: 'error.log', level: 'error' }),
//     new winston.transports.File({ filename: 'logs/logs.log' }),
//     new winston.transports.Console({
//         level: 'info',
//         format: winston.format.combine(
//           winston.format.colorize(),
//           winston.format.simple()
//         )
//       })
//   ],
// });

const Files = require('./Files')

class Logger {
    constructor() {
        this.logs = [];
    }

    log(value) {
        this.logs.push(value)
        console.log(value)
    }

    save(name) {
        Files.save(name, this.logs.join(';'))
        this.logs = []
    }

    saveSync(name) {
        Files.saveSync(name, this.logs.join(';'))
        this.logs = []
    }
}
module.exports = Logger