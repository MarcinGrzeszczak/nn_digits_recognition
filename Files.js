const fs = require('fs');
const {createCanvas} = require('canvas');

const fsProm = fs.promises

function save(name, value) {
    const objectToSave = Object.create({data: value});
    const serializedValue = JSON.stringify(objectToSave.data);
    fs.writeFile(name, serializedValue, err => console.log(err));
}

function saveSync(name, value) {
    const serializedValue = JSON.stringify(value);
    fs.writeFileSync(name, serializedValue, err => console.log(err));
}

async function read(name) {
    const data = await fsProm.readFile(name, 'utf8')
    return JSON.parse(data);
}

function isFileExists(name) {
    return fs.existsSync(name);
}

function readStream(path) {
   return fs.createReadStream(path)
}

function saveCanvas(name, data) {
    const canvas = createCanvas(28,28)
    const ctx = canvas.getContext('2d');

    const imgData = ctx.createImageData(28,28);
    let imgIndex = 0;
    for (let i = 0; i < data.length; i++) {
      imgData.data[imgIndex++] = 255;
      imgData.data[imgIndex++] = 255;
      imgData.data[imgIndex++] = 255;
      imgData.data[imgIndex++] = data[i] * 255;
    }

    ctx.putImageData(imgData,0,0);
    const base = canvas.toDataURL();
    const img = base.replace(/^data:image\/\w+;base64,/, "");
    const buf = Buffer.from(img, 'base64');
    fs.writeFileSync(name, buf, err => console.log(err));
}

module.exports = {
    save,
    saveSync,
    read,
    readStream,
    isFileExists,
    saveCanvas
}