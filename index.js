const process = require('process');
const path = require('path');
const DataLoader = require('./DataLoader');
const Files = require('./Files');
const mathjs = require('mathjs')
const NeuralNetwork = require('./NeuralNetwork');
const NeuralNetwork2 = require('./NeuralNetwork2')
const express = require('express')

const app = express()

const PARSED_TRAIN_PATH =  path.join(process.cwd(), 'dataset', 'parsedTrain.data');
const TRAIN_DATA_CSV_PATH = path.join(process.cwd(), 'dataset', 'train.csv');

const PARSED_TEST_PATH =  path.join(process.cwd(), 'dataset', 'parsedTest.data');
const TEST_DATA_CSV_PATH = path.join(process.cwd(), 'dataset', 'test2.csv');

const WEIGHTS_PATH = path.join(process.cwd(), 'weights.data');

const nn = new NeuralNetwork(784,200,80,10, 15 , .01);
//const nn = new NeuralNetwork2(784,800,10, 3 , .01);
if(Files.isFileExists(WEIGHTS_PATH)) {
    nn.loadWheights()
    // .then(() => app.listen(80, () => {
    //     console.log(`Started App at 80`)
    //   }))
   .then(() => DataLoader.loadData(PARSED_TRAIN_PATH, TRAIN_DATA_CSV_PATH))
   .then(data => nn.train(data))
    .then(()=>DataLoader.loadData(PARSED_TEST_PATH, TEST_DATA_CSV_PATH))
    .then(data => nn.test(data));
 } else {
    DataLoader.loadData(PARSED_TRAIN_PATH, TRAIN_DATA_CSV_PATH)
    .then(data => nn.train(data))
    .then(()=>DataLoader.loadData(PARSED_TEST_PATH, TEST_DATA_CSV_PATH))
    .then(data => nn.test(data));
}

app.use(express.json());
app.use(express.static('website'))

app.post('/predict', async (req, res) => {
    const input = req.body.data;
    Files.saveCanvas('request.jpg', input)
    const predict = nn.predict(input)
    console.log(predict)
    const maxval = predict._data[0].findIndex(val => val === mathjs.max(predict))
    res.send({data: predict._data[0], maxval})
});

